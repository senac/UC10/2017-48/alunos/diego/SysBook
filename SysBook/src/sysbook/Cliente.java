
package sysbook;


public class Cliente {
   private String nome;
   private String cpf;
   private String rg;
   private String telefone;
   private String email;
   
   
   public Cliente(){
   }
   
    public Cliente (String nome, String cpf, String rg, String telefone, String email){
        this.nome = nome;
        this.cpf = cpf;
        this.email = email;
        this.rg = rg;
        this.telefone = telefone;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return  nome + " - " + cpf + " - " + rg + " - " + telefone + " - " + email ;
    }
    
    
    
}
